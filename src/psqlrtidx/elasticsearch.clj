(ns psqlrtidx.elasticsearch
  (:import [java.util.concurrent Executors])
  (:require [clojure.java.jdbc :as jdbc]
            [clojure.data.json :as json]
            [clojure.pprint :refer :all]
            [clojure.algo.generic.functor :refer :all]
            [psqlrtidx.customerdb :as db]
            [clojurewerkz.elastisch.rest :as rest]
            [clojurewerkz.elastisch.rest.index :as idx]
            [clojurewerkz.elastisch.rest.bulk :as bulk]
            [clojurewerkz.elastisch.rest.document :as doc]
            [clojurewerkz.elastisch.rest.response :as resp]))

(def escluster "http://elasticsearch:9200")
(def esidx "customer")
(def estype "account")

(defn clean-types [v]
  (cond
   (nil? v) v
   (= (.getClass v) java.sql.Timestamp) (str v)
   :else v))

(defn doc->bulkjson [row]
  (let [filt (fmap clean-types row)]
    (println (json/write-str {:index {:_index "customer"
                                      :_type "account"
                                      :_id (:id filt)}}))
    (println (json/write-str (dissoc filt :id)))))

(defn doc->es [row]
  (println "Indexing row" (:id row) (:name row))
  (doc/create "customer" "account" (fmap clean-types row) 
              :id (:id row)))


(defn push-bulk [items]
  (println (-> (Thread/currentThread) (.getName)) "Uploading batch of" (count items) "rows")
  (let [response (bulk/bulk (bulk/bulk-index items))]
    (println (-> (Thread/currentThread) (.getName)) "OK =" (every? resp/ok? (->> response :items (map :create))))))


(defn batch-and-send [accum pushfn row]
  (let [processed (assoc (fmap clean-types row)
                    :_index esidx :_type estype)]

    (swap! accum #(conj % processed))

    (if (>= (count @accum) 1000)
      ;; Batch-upload accumulated rows and reset
      (do (pushfn @accum)
          (reset! accum (list processed))))))

(defn push-dispatch [pool items]
  (.submit pool #(push-bulk items)))

;; Bulk loader
(defn bulk []
  (println "Connecting")
  (let [esconn (rest/connect! escluster)
        pool (Executors/newFixedThreadPool 6)
        accum (atom (list))]

    (println "Start query")
    (db/each-account 
     (fn [row] (batch-and-send accum (partial push-dispatch pool) row)))

    ;; Send any outstanding rows
    (println (apply str (repeat 40 "#")) "Sending outstanding messages")

    (push-dispatch pool @accum)

    (println "Waiting for thread-pool to shut down")
    (.shutdown pool)))
