
-- Target table
DROP TABLE IF EXISTS account;
CREATE TABLE account (
    id char(36) PRIMARY KEY,
    name character varying(255),
    email character varying(255)
);

-- Staging table; will gather updates until processed
DROP TABLE IF EXISTS updatedaccounts;
CREATE TABLE updatedaccounts (
    id char(36) PRIMARY KEY,
    op CHAR,
    updated TIMESTAMP
);


-- The function that reacts to updates
DROP FUNCTION IF EXISTS on_account_change() CASCADE;

CREATE FUNCTION on_account_change() 
    RETURNS trigger AS $on_account_change$
DECLARE
    opcode CHAR;
BEGIN

    IF TG_OP = 'DELETE' OR TG_OP = 'TRUNCATE' THEN
       opcode := 'D';
    ELSE
       opcode := 'U';
    END IF;

    -- Insert the updated account into the staging table
    IF EXISTS (SELECT 1 FROM updatedaccounts WHERE id = NEW.id) THEN
        UPDATE updatedaccounts SET op=opcode, updated=now() WHERE id=NEW.id;

    ELSE
        INSERT INTO updatedaccounts (id, op, updated) VALUES (NEW.id, opcode, now());
    END IF;

    -- Notify that the account table has been updated
    PERFORM pg_notify('accountupdate', ''||NEW.id);

    RETURN NULL;
END;
$on_account_change$ LANGUAGE plpgsql;


-- The actual change trigger
DROP TRIGGER IF EXISTS account_change_trigger ON account;
CREATE TRIGGER account_change_trigger
    AFTER INSERT OR UPDATE OR DELETE ON account
    FOR EACH ROW EXECUTE PROCEDURE on_account_change();


ALTER TABLE account OWNER TO customer;
ALTER TABLE updatedaccounts OWNER TO customer;
ALTER FUNCTION on_account_change() OWNER TO customer;
