#!/bin/bash

echo "#### Create test DB ####"

gosu postgres postgres --single <<EOF
  CREATE USER customer PASSWORD 'customer';
  CREATE DATABASE customer OWNER customer;
  GRANT ALL PRIVILEGES ON DATABASE customer TO customer;
EOF

echo "#### Initialise test DB ####"

gosu postgres postgres --single customer -j < /testschema.sql

echo
echo "#### Test DB initialised ####"
