FROM java:openjdk-7-jre

# Install netcat for waitport
RUN apt-get update && \
    apt-get install -y netcat-openbsd && \
    apt-get clean

COPY docker/waitport /usr/local/bin/

RUN curl https://raw.githubusercontent.com/technomancy/leiningen/stable/bin/lein -o /usr/local/bin/lein && \
    chmod a+rx /usr/local/bin/lein

ADD . /code
WORKDIR /code

RUN adduser --disabled-password --gecos '' transfer
USER transfer

RUN lein deps

CMD waitport elasticsearch 9200 && \
    waitport psql 5432 && \
    lein test-out junit
