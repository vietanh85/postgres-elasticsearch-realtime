(defproject psqlrtidx "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}

  :dependencies [[org.clojure/clojure "1.6.0"]
                 [org.clojure/algo.generic "0.1.1"]
                 [org.clojure/data.json "0.2.4"]
                 [org.clojure/java.jdbc "0.3.3"]
                 ;[org.postgresql/postgresql "9.2-1004-jdbc4"]
                 [com.impossibl.pgjdbc-ng/pgjdbc-ng "0.3" :classifier "complete"]
                 [clj-dbcp "0.8.1"]
                 [clojurewerkz/elastisch "1.5.0-beta3"]]

  :profiles {:dev {:plugins [[lein-test-out "0.3.1"]]}}

  :main psqlrtidx.core)
